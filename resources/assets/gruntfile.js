// Basic Grunt configuration

module.exports = function(grunt) {

    // 1. All configuration goes here

    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),

        // Sass configuration
        sass: {
            dist: {
                options: {
                    style: 'compressed'
                },
                files: {
                    'css/styles.css': 'sass/styles.scss'
                }
            }
        },

        // Minify javascript
        uglify: {
            options: {
                banner: ''
            },
            target_1: {
                src: ['js/main.js'],
                dest: 'js/main.min.js'
            }
        },

        //Reducing image sizes
        imagemin: {
            dynamic: {
                files: [{
                    expand: true,
                    cwd: 'images-orig/',
                    src: ['**/*.{png,jpg,gif}'],
                    dest: 'images/'
                }]
            }
        },
        watch: {
            sass: {
                files: ['sass/**/*.scss'],
                tasks: ['sass'],
            },
            uglify: {
                files: ['js/main.js'],
                tasks: ['uglify']
            },
            imagemin:{
                files: ['images-orig/*.{png,jpg,gif}'],
                tasks: ['imagemin']
            },
            livereload: {
                options: {
                    livereload: true
                },
                files: [
                    '../../app/views/*.php', 'css/*.css'
                ]
            },

        }
    });

    // 3. Where we tell Grunt we plan to use this plug-in.

    grunt.loadNpmTasks('grunt-contrib-sass');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-imagemin');
    grunt.loadNpmTasks('grunt-contrib-watch');

    // 4. Where we tell Grunt what to do when we type "grunt" into the terminal.

    grunt.registerTask('default', ['watch']);

};